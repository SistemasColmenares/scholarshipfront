import React, {useEffect, useState} from 'react';
import {
    Button,
    TextField,
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    MenuItem
} from '@material-ui/core';
import GridContainer from 'src/components/Grid/GridContainer';
import GridItem from 'src/components/Grid/GridItem';
import axios from 'src/utils/AxiosHelper';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import swal from 'sweetalert';

const initalData = {
    free_time: '',
    free_time_frequency: '',
    free_time_place: '',
    vacation_frequency: '',
    vacation_place: '',
    last_vacation: '',
    observation: ''
}

const Interes = ({
    family_id,
    formVerification,
    getFormVerifications
}) => {

    useEffect(() => {
        getData();
    }, [])

    //state
    const [userData, setUserData] = useState(initalData);
    const [update, setUpdate] = useState(false);

    const getData = () => {
        axios.get(`research/leisureTime/${family_id}`)
        .then(async response => {
            if((response.status === 200) && response.data.length > 0){
                await setUserData(response.data[0]);
                setUpdate(true);
            }
        })
    }

    const handleSubmit = () => {
        userData.family_id=family_id;
        var url = `research/leisureTime`;
        if(update){
            userData._method = 'put';
            url = `research/leisureTime/${family_id}`;
        }
        axios.post(url, userData)
        .then(response => {
            if(response.status === 200){
                axios.post(`research/researchFormVerification/${family_id}`, {leisure_time: 1, _method: "put"})
                .then(response => {
                    if(response.status === 200){
                        swal("Datos guardados correctamente", {icon: "success"});
                        getFormVerifications();
                    }
                })
            }
        })
        .catch(error => {
            Object.getOwnPropertyNames(error.response.data).forEach(function(val, idx, array) {
                swal(error.response.data[val][0], {icon: "error"});
            });
        })
    }

    const handleChange = async(e) => {
        setUserData({
            ...userData,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
        <form onSubmit={e => {
            e.preventDefault();
            handleSubmit();
        }}>
        <Accordion defaultExpanded>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography color="textSecondary" gutterBottom variant="h4" style={{textAlign: 'center'}}>
                    Interes social y cultural
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                {/* Datos de la familia */}
                <GridContainer  alignItems="center" justify="center" spacing={2} style={{width: '100%'}}>
                    <GridItem xs={12} md={6}>
                        <TextField
                            fullWidth margin="normal" variant="outlined" required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.free_time} name="free_time"
                            label="¿A qué dedican el tiempo libre?"
                        />
                    </GridItem>
                    <GridItem xs={12} md={6}>
                        <TextField
                            fullWidth margin="normal" variant="outlined" required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.free_time_frequency} name="free_time_frequency"
                            label="¿Con que frecuencia salen a divertirse?"
                        />
                    </GridItem>
                    <GridItem xs={12} md={6}>
                        <TextField
                            fullWidth margin="normal" variant="outlined" required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.free_time_place} name="free_time_place"
                            label="¿A dónde acostumbran a ir?"
                        />
                    </GridItem>
                    <GridItem xs={12} md={6}>
                        <TextField
                            fullWidth margin="normal" variant="outlined" required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.vacation_frequency} name="vacation_frequency"
                            label="¿Con que frecuencia salen de vacaciones?"
                        />
                    </GridItem>
                    <GridItem xs={12} md={6}>
                        <TextField
                            fullWidth margin="normal" variant="outlined" required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.vacation_place} name="vacation_place"
                            label="¿A dónde acostumbran a ir de vacaciones?"
                        />
                    </GridItem>
                    <GridItem xs={12} md={6}>
                        <TextField
                            fullWidth margin="normal" variant="outlined" required={true} 
                            onChange={(e) => handleChange(e)}
                            value={userData.last_vacation} name="last_vacation"
                            label="¿Cuando y a donde fueron la última vez?"
                        />
                    </GridItem>
                    <GridItem xs={12} md={6}>
                        <TextField
                            fullWidth margin="normal" variant="outlined" required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.observation} name="observation"
                            multiline rows={5}
                            label="Observaciones"
                        />
                    </GridItem>
                    <GridItem xs={12} style={{display: 'flex'}} alignItems="center" justify="center" >
                        <Button
                            color="primary"
                            size="large"
                            variant="contained"
                            type="submit"
                        >
                            Guardar
                        </Button>
                    </GridItem>
                </GridContainer>
            </AccordionDetails>
        </Accordion>
        </form>
        </>
    )
}

export default Interes;