import React, { useState, useEffect } from 'react';
import {
  Box,
  Container,
  makeStyles,
  Button,
  TextField,
  Typography
} from '@material-ui/core';
import Page from 'src/components/Page';
import CustomTable from 'src/components/CustomTable/CustomTable';
import axios from 'src/utils/AxiosHelper';
import Toolbar from 'src/components/CustomTable/Toolbar';
import CustomModal from 'src/components/CustomModal';
import swal from 'sweetalert';
import { CSVLink } from "react-csv";
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined';
import SearchBar from 'src/components/SearchBar';
import Cookies from 'js-cookie';
import ImageUploader from "react-images-upload";

const cols = [
    { accessor: "id", header: "ID" },
    { accessor: "name", header: "Nombre" },
    { accessor: "email", header: "Email" },
    { accessor: "created_at", header: "Creación" },
    { accessor: "updated_at", header: "Ultima Actualización" },
];

const initalData = {
    name: "",
    email: "",
    password: "",
    password_confirmation: ""
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

let baseURL = "https://api.colmenares.org.mx";
// let baseURL = "http://scholarshipsystem.test:8080";

const Empresas = () => {
    const classes = useStyles();
    const [rowsData, setRowsData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [showModal, setShowModal] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [buttonMessage, setButtonMessage] = useState("Agregar");
    const [currentUser, setCurrentUser] = useState({});
    const [excelData, setExcelData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [isSearching, setIsSearching] = useState(false);
    const [image, setImage] = useState({});
    const [prevPicture, setPrevPicture] = useState([]);
    const [userData, setUserData] = useState(initalData);
    
    const permission = Cookies.getJSON('permission');
    
    useEffect(() => {
        getUsers();
    }, [])

    const getUsers = () => {
        var aux = [["ID", "Nombre", "Email", "Creación", "Ultima Actualización"]];
        axios.get('auth/userCompany')
        .then(async response => {
            if (response.status === 200) {
                response.data.map(item => {
                    aux.push([item.id, item.name, item.email, item.created_at, item.updated_at]);
                })
                await setExcelData(aux);
                await setRowsData(response.data);
                await setIsLoading(false);
            }
        })
    }

    const handleChange = (e) => {
        setUserData({
            ...userData,
            [e.target.name]: e.target.value
        })
    }

    const onAddPress = () => {
        setUserData(initalData);
        setPrevPicture([]);
        setShowModal(true);
        setButtonMessage("Agregar");
    }

    const onEditAction = (customer) => {
        setCurrentUser(customer);
        setUserData({
            name: customer.name,
            email: customer.email,
            password: "",
            password_confirmation: ""
        });
        setPrevPicture([`${baseURL}${customer.image}`])
        setButtonMessage("Editar");
        setShowModal(true);
    }

    const onDeleteAction = async(customer) => {
        await swal({
            title: "Estas seguro?",
            text: "Una vez eliminado no se podra recuperar el usuario!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                axios.post(`auth/userCompany/${customer.id}`, {_method: "delete"})
                .then(response => {
                    swal("Usuario eliminado correctamente", {icon: "success"});
                })
            }
        });
        getUsers();
    }

    const onHandleSubmit = async() => {
        if ((userData.password.length < 6) || (userData.password_confirmation.length < 6)) {
            swal("La contraseña debe tener al menos 6 caracteres", {icon: "error"});
        }
        else {
            if (buttonMessage === "Agregar") {
                userData.image = image;
                await axios.post("auth/registerCompany", userData)
                .then(response => {
                    if(response.status === 200){
                        swal("Empresa agregada correctamente", {icon: "success"});
                    }
                })
                .catch(error => {
                    Object.getOwnPropertyNames(error.response.data).forEach(function(val, idx, array) {
                        swal(error.response.data[val][0], {icon: "error"});
                    });
                })
            }
            else {
                var aux = userData;
                aux._method = "put";
                aux.image = image;
                await axios.post(`auth/userCompany/${currentUser.id}`, aux)
                .then(response => {
                    if(response.status === 200){
                        swal("Empresa actualizada correctamente", {icon: "success"});
                    }
                })
                .catch(error => {
                    Object.getOwnPropertyNames(error.response.data).forEach(function(val, idx, array) {
                        swal(error.response.data[val][0], {icon: "error"});
                    });
                })
            }
            setShowModal(false);
            getUsers();
        }
    }

    const onChangeSearchField = (value) => {
        if (value.length > 3) {
            setTimeout(() => {
                onSearchPress(value);
            }, 500);
        }
        else {
            setIsSearching(false);
        }
    }

    const onDeleteSearch = () => {
        setIsSearching(false);
    }

    const onSearchPress = async(value) => {
        var aux = [];
        await rowsData.map(item => {
            if (
                ( item.name && item.name.toLowerCase().includes(value.toLowerCase())) ||
                ( item.email && item.email.toLowerCase().includes(value.toLowerCase()))
            ) {
                aux.push(item);
            }
        })
        await setFilteredData(aux);
        await setIsSearching(true);
    }

    const onDrop = async(picture, prevPictures) => {
        setTimeout(() => {
            setImage(picture[0]);
        }, 500);
    };

    return (
        <Page
            className={classes.root}
            title=".:BECAS - Empresas:."
        >
            <Container maxWidth={false} style={{ display: 'flex', flexDirection: 'column'}}>
                <Typography variant="h1">Empresas</Typography>
                <Toolbar onAddPress={onAddPress} />
                <SearchBar
                    onChange={(value) => {onChangeSearchField(value)}}
                    onDeleteSearch={onDeleteSearch}
                    onSearchPress={(value) => {onSearchPress(value)}}
                >
                    <Button
                        size="small"
                        style={{alignSelf: 'flex-end'}}
                        endIcon={<TableChartOutlinedIcon/>}
                    >
                        <CSVLink
                            data={excelData}
                            filename={"Empresas.csv"}
                            className="btn btn-primary"
                            target="_blank"
                            style={{height: 0, display: 'flex', color: '#263238',justifyContent: 'center', alignItems: 'center' }}
                        >
                        Exportar
                        </CSVLink>
                    </Button>
                </SearchBar>
                <Box mt={3}>
                    {
                        !isLoading &&
                        <CustomTable
                            cols={cols}
                            rows={isSearching? filteredData : rowsData}
                            data={isSearching? filteredData : rowsData}
                            showAction={false}
                            actions={true}
                            updateAction={(permission.update === 1) ? true : false}
                            delAction={(permission.delete === 1) ? true : false}
                            onEdit={(customer) => {onEditAction(customer)}}
                            onDelete={(customer) => {onDeleteAction(customer)}}
                        />
                    }
                </Box>
            </Container>
            <CustomModal
                open={showModal}
                handleClose={ () => setShowModal(false) }
            >
                <Box mb={3}>
                    <Typography
                        color="textPrimary"
                        variant="h2"
                    >
                        Editar empresa
                    </Typography>
                </Box>
                <TextField
                    fullWidth
                    label="Nombre"
                    margin="normal"
                    name="name"
                    onChange={(e) => handleChange(e)}
                    value={userData.name}
                    variant="outlined"
                />
                <TextField
                    fullWidth
                    label="Email"
                    margin="normal"
                    name="email"
                    onChange={(e) => handleChange(e)}
                    value={userData.email}
                    variant="outlined"
                />
                <TextField
                    fullWidth
                    label="Contraseña"
                    margin="normal"
                    name="password"
                    type="password"
                    required={true}
                    onChange={handleChange}
                    value={userData.password}
                    variant="outlined"
                />
                <TextField
                    fullWidth
                    label="Confirmar contraseña"
                    margin="normal"
                    name="password_confirmation"
                    required={true}
                    type="password"
                    onChange={(e) => handleChange(e)}
                    value={userData.password_confirmation}
                    variant="outlined"
                />
                <Typography
                    color="textPrimary"
                    variant="h4"
                    style={{textAlign: 'center', width: '100%'}}
                >
                    Firma
                </Typography>
                <ImageUploader
                    withIcon={true}
                    buttonText={"Seleccionar imagen"}
                    label=""
                    withPreview
                    singleImage
                    defaultImages={prevPicture}
                    onChange={onDrop}
                    imgExtension={[".jpg", ".gif", ".png"]}
                    maxFileSize={5242880}
                />
                <Box my={2}>
                    <Button
                        color="primary"
                        disabled={isSubmitting}
                        size="large"
                        variant="contained"
                        onClick={onHandleSubmit}
                    >
                        {buttonMessage}
                    </Button>
                </Box>
            </CustomModal>
        </Page>
    );
};

export default Empresas;