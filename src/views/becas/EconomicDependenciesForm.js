import React, {useEffect, useState} from 'react';
import {
    Button,
    TextField,
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Box
} from '@material-ui/core';
import GridContainer from 'src/components/Grid/GridContainer';
import GridItem from 'src/components/Grid/GridItem';
import axios from 'src/utils/AxiosHelper';
import Cookies from 'js-cookie';
import CustomModal from 'src/components/CustomModal';
import Table from 'src/components/CustomTable/CustomTable';
import swal from 'sweetalert';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const cols = [
    {accessor: "name", header: "Nombre"},
    {accessor: "kinship", header: "Parentesco"},
    {accessor: "ocupation", header: "Ocupación"},
    {accessor: "age", header: "Edad"}
];

const initalData = {
    name: "",
    age: "",
    kinship: "",
    ocupation: ""
}

const EconomicDependenciesForm = ({
    family_id,
    formVerification,
    getFormVerifications,
    estudios = false,
    observations= {}
}) => {

    useEffect(() => {
        getFamilyData();
        if(estudios){
            setReason(observations.economic_dependent);
        }
    }, [])

    //state
    const [studetns, setStudetns] = useState([]);
    const [showForm, setShowForm] = useState(false);
    const [userData, setUserData] = useState(initalData);
    const [reason, setReason] = useState('');

    const handleSubmit = () => {
        swal("Datos de los alumnos actualizados correctamente", {icon: "success"});
    }

    const handleChange = (e) => {
        setUserData({
            ...userData,
            [e.target.name]: e.target.value
        })
    }

    const getFamilyData = () => {
        axios.get(`family/economicDependent/${family_id}`)
        .then(response => {
            if(response.status === 200){
                setStudetns(response.data);
            }
        })
    }

    const onDeleteAction = async(customer) => {
        await swal({
            title: "Estas seguro?",
            text: "Una vez eliminado no se podra recuperar el alumno!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                axios.post(`family/economicDependent/${customer.id}`, {_method: "delete"})
                .then(response => {
                    swal("Alumno eliminado correctamente", {icon: "success"});
                    getFamilyData();
                })
            }
        });
    }

    const handleNewDependency = () => {
        setShowForm(true);
    }

    const onSaveNewStudent = () => {
        userData.family_id = family_id;
        axios.post(`family/economicDependent`, userData)
        .then(response => {
            if(response.status === 200){
                swal("Alumno agregado correctamente", {icon: "success"});
                setUserData(initalData);
                getFamilyData();
            }
        })
        .catch(error => {
            Object.getOwnPropertyNames(error.response.data).forEach(function(val, idx, array) {
                swal(error.response.data[val][0], {icon: "error"});
            });
        });
        setShowForm(false);
    }

    const submitReason = () => {
        axios.post(`research/observation/${family_id}`, { economic_dependent: reason }, 'put')
        .then(response => {
            if(response.status === 200){
                axios.post(`research/researchFormVerification/${family_id}`, {economic_dependent: 1}, 'put')
                .then(response => {
                    if(response.status === 200){
                        swal("Correcto", "Observaciones guardadas de manera correcta", "success");
                    }
                })
            }
        })
    }

    return (
        <>
        <Accordion defaultExpanded={!estudios}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography color="textSecondary" gutterBottom variant="h4" style={{textAlign: 'center'}}>
                    Datos dependientes economicos
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                {/* Datos de la familia */}
                <GridContainer  alignItems="center" justify="center" spacing={2} style={{width: '100%'}}>
                    <GridItem xs={12} >
                        <Table 
                            cols={cols}
                            rows={studetns}
                            actions={!estudios}
                            showAction={false}
                            updateAction={false}
                            onDelete={customer => onDeleteAction(customer)}
                        />
                    </GridItem>
                    {!estudios && <GridItem xs={6} style={{display: 'flex'}} alignItems="center" justify="center" >
                        <Button
                            color="primary"
                            size="large"
                            variant="contained"
                            onClick={handleNewDependency}
                        >
                            Agregar
                        </Button>
                    </GridItem>}
                    {!estudios && <GridItem xs={6} style={{display: 'flex'}} alignItems="center" justify="center" >
                        <Button
                            color="primary"
                            size="large"
                            variant="contained"
                            onClick={handleSubmit}
                        >
                            Guardar
                        </Button>
                    </GridItem>}
                </GridContainer>
            </AccordionDetails>
        </Accordion>
        {estudios && 
        <form onSubmit={(e) => {
            e.preventDefault();
            submitReason();
        }}>
            <Accordion defaultExpanded>
                <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography color="textSecondary" gutterBottom variant="h4" style={{textAlign: 'center'}}>
                        Observaciones dependientes economicos
                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <GridContainer direction="row" alignItems="center" justify="center" spacing={2} style={{width: '100%'}}>
                        <GridItem xs={12} >
                            <TextField
                                fullWidth
                                label="Observaciones"
                                margin="normal" name="reason"
                                required={true} multiline rows={3}
                                onChange={(e) => setReason(e.target.value)}
                                value={reason}
                                variant="outlined"
                            />
                        </GridItem>
                        <GridItem xs={12} style={{display: 'flex'}} alignItems="center" justify="center" >
                            <Button
                                color="primary"
                                size="large"
                                variant="contained"
                                type="submit"
                            >
                                Guardar
                            </Button>
                        </GridItem>
                    </GridContainer>
                </AccordionDetails>
            </Accordion>
        </form>}
        <CustomModal
            open={showForm}
            handleClose={ () => {
                setShowForm(false);
                setUserData(initalData);
            }}
        >
            <Box mb={3}>
                <Typography
                    color="textPrimary"
                    variant="h2"
                >
                    Alta de usuario
                </Typography>
            </Box>
            <TextField
                fullWidth
                label="Nombre"
                margin="normal"
                name="name"
                required={true}
                onChange={(e) => handleChange(e)}
                value={userData.name}
                variant="outlined"
            />
            <TextField
                fullWidth
                label="Edad"
                margin="normal"
                name="age"
                required={true}
                onChange={(e) => handleChange(e)}
                value={userData.age}
                variant="outlined"
            />
            <TextField
                fullWidth
                label="Parentesco"
                margin="normal"
                name="kinship"
                required={true}
                onChange={handleChange}
                value={userData.kinship}
                variant="outlined"
            />
            <TextField
                fullWidth
                label="Ocupación"
                margin="normal"
                name="ocupation"
                required={true}
                onChange={(e) => handleChange(e)}
                value={userData.ocupation}
                variant="outlined"
            />
            <Box my={2}>
                <Button
                    color="primary"
                    size="large"
                    variant="contained"
                    onClick={onSaveNewStudent}
                >
                    Agregar
                </Button>
            </Box>
        </CustomModal>
        </>
    )
}

export default EconomicDependenciesForm;