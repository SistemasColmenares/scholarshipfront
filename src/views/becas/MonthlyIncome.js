import React, {useEffect, useState} from 'react';
import {
    Button,
    TextField,
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    MenuItem
} from '@material-ui/core';
import GridContainer from 'src/components/Grid/GridContainer';
import GridItem from 'src/components/Grid/GridItem';
import axios from 'src/utils/AxiosHelper';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import swal from 'sweetalert';

const initalData = {
    family_id: '',
    father: 0,
    mother: 0,
    pantry_voucher: 0,
    other: 0,
    total: 0,
}

const MonthlyIncome = ({
    family_id,
    formVerification,
    getFormVerifications,
    estudios = false,
    observations= {}
}) => {

    useEffect(() => {
        getUsersData();
        if(estudios){
            setReason(observations.monthly_income);
        }
    }, [])

    //state
    const [userData, setUserData] = useState(initalData);
    const [requestType, setRequestType] = useState(0); // 0 post 1 put
    const [reason, setReason] = useState('');

    const handleSubmit = () => {
        userData.family_id=family_id;
        userData.total = getTotal();
        Object.getOwnPropertyNames(userData).forEach(function(val, idx, array) {
            if( (val !== 'total') && (val !== 'family_id')){
                if(userData[val] !== ''){
                    userData[val]= parseFloat(userData[val]).toFixed(2);
                }
                else{
                    userData[val] = parseFloat('0.00').toFixed(2);
                }
            }
        });
        var url = 'family/monthlyIncome';
        if(requestType===1){
            userData._method = "put";
            url += `/${family_id}`
        }
        axios.post(url, userData)
        .then(response => {
            if(response.status === 200){
                axios.post(`family/formVerification/${family_id}`, {monthly_income: 1, _method: "put"})
                .then(response => {
                    if(response.status === 200){
                        swal("Datos de los alumnos actualizados correctamente", {icon: "success"});
                        getFormVerifications();
                    }
                })
            }
        })
        .catch(error => {
            Object.getOwnPropertyNames(error.response.data).forEach(function(val, idx, array) {
                swal(error.response.data[val][0], {icon: "error"});
            });
        })
    }

    const handleChange = async(e) => {
        if((parseFloat(e.target.value)>=0) || (e.target.value === '')){
            setUserData({
                ...userData,
                [e.target.name]: e.target.value
            })
        }
    }

    const getUsersData = () => {
        axios.get(`family/monthlyIncome/${family_id}`)
        .then(response => {
            if(response.status === 200){
                if(response.data.length > 0){
                    setRequestType(1);
                    setUserData(response.data[0]);
                }
            }
        })
    }

    const submitReason = () => {
        axios.post(`research/observation/${family_id}`, { monthly_income: reason }, 'put')
        .then(response => {
            if(response.status === 200){
                axios.post(`research/researchFormVerification/${family_id}`, {monthly_income: 1}, 'put')
                .then(response => {
                    if(response.status === 200){
                        swal("Correcto", "Observaciones guardadas de manera correcta", "success");
                    }
                })
            }
        })
    }

    const getTotal = () => {
        var aux = 0;
        Object.getOwnPropertyNames(userData).forEach(function(val, idx, array) {
            if( (val !== 'other_description') && (val !== 'total') && (val !== 'family_id') && (val !== 'created_at') && (val !== 'updated_at') && (val !== 'id')){
                if(userData[val] !== ''){
                    aux += parseFloat(userData[val]);
                }
            }
        });
        return aux.toFixed(2);
    }

    return (
        <>
        <form onSubmit={e => {
            e.preventDefault();
            handleSubmit();
        }}>
        <Accordion defaultExpanded={!estudios}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography color="textSecondary" gutterBottom variant="h4" style={{textAlign: 'center'}}>
                    Ingresos mensuales
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                {/* Datos de la familia */}
                <GridContainer  alignItems="center" justify="center" spacing={2} style={{width: '100%'}}>
                    <GridItem xs={12} >
                        <TextField
                            disabled={estudios}
                            fullWidth
                            label="Ingresos del padre (después de impuestos)"
                            margin="normal"
                            type="number"
                            name="father"
                            required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.father}
                            variant="outlined"
                        />
                    </GridItem>
                    <GridItem xs={12} >
                        <TextField
                            disabled={estudios}
                            fullWidth
                            label="Ingresos de la madre (después de impuestos)"
                            margin="normal"
                            name="mother"
                            type="number"
                            required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.mother}
                            variant="outlined"
                        />
                    </GridItem>
                    <GridItem xs={12} >
                        <TextField
                            disabled={estudios}
                            fullWidth
                            label="Ingresos en vales de despensa"
                            margin="normal"
                            type="number"
                            name="pantry_voucher"
                            required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.pantry_voucher}
                            variant="outlined"
                        />
                    </GridItem>
                    <GridItem xs={12} >
                        <TextField
                            disabled={estudios}
                            fullWidth
                            label="Otros ingresos"
                            margin="normal"
                            name="other"
                            type="number"
                            required={true}
                            onChange={(e) => handleChange(e)}
                            value={userData.other}
                            variant="outlined"
                        />
                    </GridItem>
                    <GridItem xs={12} >
                        <TextField
                            fullWidth
                            label="Total"
                            type="number"
                            margin="normal"
                            name="total"
                            disabled
                            required={true}
                            onChange={(e) => handleChange(e)}
                            value={getTotal()}
                            variant="outlined"
                        />
                    </GridItem>
                    {!estudios && <GridItem xs={12} style={{display: 'flex'}} alignItems="center" justify="center" >
                        <Button
                            color="primary"
                            size="large"
                            variant="contained"
                            type="submit"
                        >
                            Guardar
                        </Button>
                    </GridItem>}
                </GridContainer>
            </AccordionDetails>
        </Accordion>
        </form>
        {estudios && 
        <form onSubmit={(e) => {
            e.preventDefault();
            submitReason();
        }}>
            <Accordion defaultExpanded>
                <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography color="textSecondary" gutterBottom variant="h4" style={{textAlign: 'center'}}>
                        Observaciones ingresos mensuales
                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <GridContainer direction="row" alignItems="center" justify="center" spacing={2} style={{width: '100%'}}>
                        <GridItem xs={12} >
                            <TextField
                                fullWidth
                                label="Observaciones"
                                margin="normal" name="reason"
                                required={true} multiline rows={3}
                                onChange={(e) => setReason(e.target.value)}
                                value={reason}
                                variant="outlined"
                            />
                        </GridItem>
                        <GridItem xs={12} style={{display: 'flex'}} alignItems="center" justify="center" >
                            <Button
                                color="primary"
                                size="large"
                                variant="contained"
                                type="submit"
                            >
                                Guardar
                            </Button>
                        </GridItem>
                    </GridContainer>
                </AccordionDetails>
            </Accordion>
        </form>}
        </>
    )
}

export default MonthlyIncome;