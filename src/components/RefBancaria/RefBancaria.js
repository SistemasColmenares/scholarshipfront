import React, {useEffect, useState} from 'react';
import { Page, Text, View, Document, Image, StyleSheet, PDFDownloadLink, PDFViewer} from '@react-pdf/renderer';
import { Link as RouterLink, useNavigate, useParams } from 'react-router-dom';
import axios from 'src/utils/AxiosHelper';
//Images
import talonPago from 'src/icons/talon_pago.jpg'
import refHeader from 'src/icons/refHeader.jpg'
import banorte from 'src/icons/banorte.jpg'

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  section: {
    margin: 10,
    flexGrow: 1,
    flexDirection: 'column'
  },
  title: {
      fontSize: 18,
      textAlign: 'center'
  },
  negritas: {
    fontWeight: '800',
    fontSize: 10
  },
  negritas_italic: {
    fontWeight: '800',
    fontSize: 10,
    fontStyle: 'italic'
  },
  table: {
      display: 'flex',
      width: '60%',
      flexDirection: 'row',
      flexWrap: 'wrap'
  },
  tableContent: {
      border: '1',
      borderStyle: 'solid',
      borderColor: 'black',
      width: '50%',
      height: 20,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'flex-start',
      paddingLeft: 10
  },
  tableRow: {
      border: '1',
      borderStyle: 'solid',
      borderColor: 'black',
      width: '100%',
      height: 20,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'space-around',
      flexDirection: 'row',
      paddingLeft: 10
  },
  tableText: {
      fontSize: 8
  }
});


// Create Document Component
const MyDocument = ({settings, family}) => {
    return(
        <Document>
        
            <Page size="A4" style={styles.page}>
                <View style={styles.section}>
                    <View style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'flex-start'}} >
                        <Image src={refHeader} style={{width: '100%'}}/>
                    </View>
                    <View style={{margin: 10, marginLeft: 60}}>
                        <Text style={styles.negritas} >
                            Estimados padres de familia, ponemos a su disposición los siguientes medios de pago para su estudio socioeconómico. 
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 60}}>
                        <Text style={styles.negritas} >
                            1. Transferencia electrónica
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 75}}>
                        <Text style={{fontWeight: '400', fontSize: 10}} >
                            Derivado de la contingencia epidemiológica generada por el virus COVID-19, 
                            hemos habilitado la opción de pago vía transferencia electrónica de fondos también
                            para el estudio socioeconómico, como parte del trámite de solicitud de beca para el
                            ciclo escolar {new Date().getFullYear()} - {new Date().getFullYear()+1}.
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 75}}>
                        <Text style={{fontWeight: '400', fontSize: 10}} >
                            Para realizar el pago por este medio es necesario toman en cuenta lo siguiente:
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 105}}>
                        <Text style={{fontWeight: '400', fontSize: 10}} >
                            a. Considerar que el pago del estudio socioeconómico es de ${settings.research_cost}
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 105}}>
                        <Text style={{fontWeight: '400', fontSize: 10}} >
                            b. En el portal de su banco realizar la transferencia electrónica de fondos.
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 120}}>
                        <Text style={{fontWeight: '400', fontSize: 10}} >
                            *Si el pago se realiza de Banorte se deberá tomar la cuenta bancaría {settings.account}, a
                            nombre de Liceo del Valle A.C
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 120}}>
                        <Text style={{fontWeight: '400', fontSize: 10}} >
                            *En caso, que el pago se realice de una institución bancaría diferente a Banorte,
                            se deberá de tomar la clave interbancaria {settings.clabe}, a nombre de Licel del Valle A.C
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 105}}>
                        <Text style={styles.negritas_italic} >
                            c. En el concepto del pago, indicar el usuario con el que ingresa a su solicitud en
                            línea y los apellidos de su familia. Se deberá realizar sólo un pago por familia.
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 105}}>
                        <Text style={styles.negritas_italic} >
                            d. Una vez realizado el pago, es necesario llenar el formato para aplicación de pago de estudios socioeconómico.
                        </Text>
                    </View>
                    <View style={{margin: 10, marginLeft: 105}}>
                        <Text style={styles.negritas_italic} >
                            e. Por ultimo, es necesario enviar el pago y el formato para aplicación de estudio socioeconómico,
                            vía mail a la dirección de correo electrónico jesus.herrera@colmenares.org.mx
                        </Text>
                    </View>
                </View>
            </Page>
            
            <Page size="A4" style={styles.page}>
                <View style={styles.section} >
                    <View style={{fontSize: 10, margin: 10}}>
                        <Text>Opción 1: Transferencia bancaría</Text>
                    </View>
                    <View style={styles.title}>
                        <Text>FORMATO PARA APLICACIÓN DE PAGO ESTUDIO</Text>
                    </View>
                    <View style={styles.title}>
                        <Text>SOCIOECONOMICO</Text>
                    </View>
                    <View style={{display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%'}} >
                        <View style={styles.table}>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >CLABE interbancaria</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >{settings.clabe}</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >Cuenta:</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >{settings.account}</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >Fecha:</Text>
                            </View>
                            <View style={styles.tableContent}>
                            </View>
                            {/* <View style={styles.tableContent}>
                                <Text style={styles.tableText} >Usuario de solicitud en línea*</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >{family.user.code}</Text>
                            </View> */}
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >Nombre de la familia</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} > {` ${family.ft_lastname} ${family.mt_lastname}`} </Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} > Banco origen: </Text>
                            </View>
                            <View style={styles.tableContent}>
                            </View>
                        </View>
                    </View>
                    <View style={{fontSize: 10, textAlign: 'center'}}>
                        <Text>*Favor de comunicarse a su colegio en caso de no contar con él.</Text>
                    </View>
                </View>

                <View style={styles.section} >
                    <View style={{fontSize: 10, margin: 10}}>
                        <Text>Opción 2: Pago en ventanilla</Text>
                    </View>
                    <View style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'flex-start', margin: 10}} >
                        <Image src={talonPago} style={{width: '100%'}}/>
                    </View>
                    <View style={{display: 'flex', justifyContent: 'center', alignItems: 'center', margin: 10}} >
                        <Image src={banorte} style={{height: 80}}/>
                    </View>
                    <View style={{display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', marginVertical: 10}} >
                        <View style={styles.table}>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >Empresa: {settings.bank_reference}</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >Referencia: {family.bank_reference}</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} >Monto: ${settings.research_cost}</Text>
                            </View>
                            <View style={styles.tableContent}>
                                <Text style={styles.tableText} > Familia {`${family.ft_lastname} ${family.mt_lastname}`}</Text>
                            </View>
                            <View style={styles.tableRow}>
                                <View style={{width: '50%'}} >
                                    <Text style={[styles.tableText, {textAlign: 'left'}]} >Conepto</Text>
                                </View>
                                <View style={{width: '50%'}} >
                                    <Text style={[styles.tableText, {textAlign: 'right', paddingRight: 10}]} >Total</Text>
                                </View>
                            </View>
                            <View style={[styles.tableRow, {borderWidth: 0}]}>
                                <View style={{width: '50%'}} >
                                    <Text style={[styles.tableText, {textAlign: 'left'}]} >Estudio socioeconómico</Text>
                                </View>
                                <View style={{width: '50%'}} >
                                    <Text style={[styles.tableText, {textAlign: 'right', paddingRight: 10}]} >${settings.research_cost}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{fontSize: 10, margin: 10, textAlign: 'center'}}>
                        <Text>*Pago no válido después de: {settings.research_date}</Text>
                    </View>
                </View>
                <View>
                </View>
            </Page>

        </Document>
    );
}

const DownloadLink = (props) => {

    //state
    const [family, setFamily] = useState({})
    const [settings, setSettings] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    
    let { family_id, schoolarship } = useParams();

    useEffect(() => {
        getData();
    }, [])

    const getData = async() => {
        await axios.get(`family/information/${family_id}`)
        .then(async response => {
            if(response.status === 200){
                await setFamily(response.data);
            }
        });
        await axios.get(`auth/settings`)
        .then(async response => {
            if(response.status === 200){
                await setSettings(response.data);
                setIsLoading(false);
            }
        });
    }

    return (
        <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
            {!isLoading && 
            <PDFViewer style={{width: '100vw', height: '80vh', marginBottom: 15}} >
                <MyDocument 
                    family={family}
                    settings={settings}
                />
            </PDFViewer>
            }
            {!isLoading && 
                <PDFDownloadLink 
                    document={
                        <MyDocument 
                            family={family}
                            settings={settings}
                        />
                    }
                    style={{backgroundColor: '#415f8e', color: 'white', minWidth: 150, padding: 15, marginTop: 10, borderRadius: 5}}
                    fileName="Referencia.pdf">
                    {({ blob, url, loading, error }) => (loading ? 'Cargando documento...' : 'Descargar documento')}
                </PDFDownloadLink>
            }
        </div>
    )
}

export default DownloadLink;